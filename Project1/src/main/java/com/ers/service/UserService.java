package com.ers.service;

import com.ers.DAO.UserDao;
import com.ers.model.User;

public class UserService {
	
	UserDao userDao= new UserDao();
	
	public boolean verifyLoginCredentials(String uName, String Pwd)
	{
		return userDao.validateUser(uName, Pwd);
		
		//return false;
		
	}
	
	public User findUser(String uName,String pwd)
	{
		return userDao.findByUser(uName);
	}
	
	public User registerUser(User user)
	{
		return userDao.save(user);
	}

}
