package com.ers.DAO;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ers.model.Reimbursement;
import com.ers.model.User;
import com.ers.util.ConnectionUtil;

public class ReimbursementDao implements DAO<Reimbursement, Integer> {

	public final static Logger log = Logger.getLogger(UserDao.class);

	public List<Reimbursement> findAll() {
		// TODO Auto-generated method stub

		List<Reimbursement> reimb_List = new ArrayList<Reimbursement>();
		String sql = "select * from ERS_REIMBURSEMENT";

		try {
			Connection con = ConnectionUtil.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet res = ps.executeQuery();
			while (res.next()) {
				reimb_List.add(new Reimbursement(res.getInt(1), res.getInt(2), res.getTimestamp(3), res.getTimestamp(4),
						res.getString(5), res.getInt(6), res.getInt(7), res.getInt(8), res.getInt(9)));
			}

		}

		catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reimb_List;
	}

	public Reimbursement findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Reimbursement save(Reimbursement obj) { // TODO Auto-generated methodstub

		// TODO Auto-generated method stub

		try {

			Connection con = ConnectionUtil.getConnection();
			String SQL = "{? = call INSERT_ERS_REIMBURSEMENT(?,?,?,?,?,?)}";
			String[] keyNames = { "reimb_id" };
			CallableStatement cs = con.prepareCall(SQL);
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setInt(2, obj.getReimb_amount());
			cs.setString(3, obj.getReimb_description());
			cs.setInt(4, obj.getReimb_author());
			cs.setInt(5, obj.getReimb_resolver());
			cs.setInt(6, obj.getReimb_status_id());
			cs.setInt(7, obj.getReimb_type_id());

			cs.execute();
			obj.setReimb_id(cs.getInt(1));
			log.info(obj);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated catch block e.printStackTrace(); }
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return obj;
	}
	// return null; }

	public List<Reimbursement> retrieveReimbursements(User user) {
		List<Reimbursement> list = new ArrayList<Reimbursement>();

		return list;

	}

	public Reimbursement update(Reimbursement obj) {
		// TODO Auto-generated method stub
		return null;
	}

	public void delete(Reimbursement obj) {
		// TODO Auto-generated method stub

	}

	/*
	 * public List<Reimbursement> findAll() { // TODO Auto-generated method stub
	 * return null; }
	 */

	/*
	 * public Reimbursement save(Reimbursement obj) { // TODO Auto-generated method
	 * stub return null; }
	 */

}
