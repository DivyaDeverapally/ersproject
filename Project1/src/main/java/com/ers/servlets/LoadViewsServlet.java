package com.ers.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoadViewsServlet
 */
public class LoadViewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadViewsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("LoadViewsServlet get method");
		System.out.println("Divya" + request.getRequestURI());
		System.out.println(response);
		String resourcepath = "HTML/" + process(request, response) + ".html";
		System.out.println("Divya process method  is " + resourcepath);
		request.getRequestDispatcher(resourcepath).forward(request, response);/// Project1/HTML/home.view
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	

	static String process(HttpServletRequest req, HttpServletResponse resp) {
		
		
		switch(req.getRequestURI()) {
			
		case "/Project1/login.view":
			return "login";
			
		  case "/Project1/home.view":
			  return "home";
		 
		
		
		
	
			
		}
		
		return null;
		
	}

}
