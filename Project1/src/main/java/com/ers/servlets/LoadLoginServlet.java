package com.ers.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ers.model.User;
import com.ers.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;

/**
 * Servlet implementation class LoadLoginServlet
 */
@WebServlet("/login")
public class LoadLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UserService userService = new UserService();

	// verifyLoginCredentials
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadLoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("In LoadLoginServlet Get Method");
		//request.getRequestDispatcher("HTML/welcome.html").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("In LoadLoginServlet POST Method");

		ObjectMapper mapper = new ObjectMapper();

		User u = mapper.readValue(request.getInputStream(), User.class);
		System.out.println(u);
		u = userService.findUser(u.getUserName(), u.getUserPwd());
		if (u != null) {
			System.out.println("got USerDAO");
			PrintWriter pw = response.getWriter();
			response.setContentType("appllication/json");
		      response.setCharacterEncoding("UTF-8");
		      if(u.getUserRoleId()==1)
		      {
		    	pw.write("employee");
		    	
		      }
		      else if(u.getUserRoleId()== 2)
		      {
		    	 pw.write("manager"); 
		      }
		      // pw.write("mona");
		       //pw.close();
			//response.sendRedirect("HTML/welcome.html");
		     //  response.sendError(0, getServletInfo());
		} else {
			
			PrintWriter pw = response.getWriter();
		//	response.setCharacterEncoding(getServletInfo());
			pw.write("invalid");

		}

	}

}
